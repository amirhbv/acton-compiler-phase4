outDir="tempout"
inputFile="samples/test.act"

rm -rf output
rm -rf jasminOut

find . -name "*.java" -print | xargs javac -classpath /usr/local/lib/antlr-4.7.2-complete.jar -d ${outDir}
java -classpath ./${outDir}:/usr/local/lib/antlr-4.7.2-complete.jar main.Acton ${inputFile}

rm -rf ${outDir}

mkdir -p jasminOut
java -jar /usr/local/lib/jasmin.jar -g -d jasminOut output/*.j

cd jasminOut
java -noverify Main