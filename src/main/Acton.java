package main;

import main.ast.node.Program;
import main.compileError.CompileErrorException;
//import main.visitor.astPrinter.ASTPrinter;
import main.visitor.codeGenerator.ActorCodeGenerator;
import main.visitor.codeGenerator.DefaultCodeGenerator;
import main.visitor.codeGenerator.HandlerCodeGenerator;
import main.visitor.codeGenerator.MainCodeGenerator;
import main.visitor.nameAnalyser.NameAnalyser;
import main.visitor.typeChecker.TypeChecker;
import org.antlr.v4.runtime.*;
import main.parsers.actonLexer;
import main.parsers.actonParser;


import java.io.IOException;

// Visit https://stackoverflow.com/questions/26451636/how-do-i-use-antlr-generated-parser-and-lexer
public class Acton {
    public static void main(String[] args) throws IOException {
        CharStream reader = CharStreams.fromFileName(args[0]);
        actonLexer lexer = new actonLexer(reader);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        actonParser parser = new actonParser(tokens);
        try {
            Program program = parser.program().p; // program is starting production rule

            NameAnalyser nameAnalyser = new NameAnalyser();
            nameAnalyser.visit(program);
            if (nameAnalyser.numOfErrors() > 0) {
                throw new CompileErrorException();
            }

//          TODO: comment out type checker
            TypeChecker typeChecker = new TypeChecker();
            typeChecker.visit(program);
            if (typeChecker.numOfErrors() > 0) {
                throw new CompileErrorException();
            }

            String outputDir = "./output"; // TODO: get output directory from program args
            program.accept(new DefaultCodeGenerator(outputDir));
            program.accept(new ActorCodeGenerator(outputDir));
            program.accept(new HandlerCodeGenerator(outputDir));
            program.accept(new MainCodeGenerator(outputDir));
        } catch (CompileErrorException ignored) {
        }
    }
}
