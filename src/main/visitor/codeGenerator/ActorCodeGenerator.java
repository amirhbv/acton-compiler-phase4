package main.visitor.codeGenerator;

import main.ast.node.Program;
import main.ast.node.declaration.ActorDeclaration;
import main.ast.node.declaration.VarDeclaration;
import main.ast.node.declaration.handler.HandlerDeclaration;
import main.ast.node.declaration.handler.InitHandlerDeclaration;
import main.ast.node.declaration.handler.MsgHandlerDeclaration;
import main.ast.node.statement.Statement;
import main.ast.type.Type;
import main.ast.type.arrayType.ArrayType;
import main.ast.type.primitiveType.BooleanType;
import main.ast.type.primitiveType.IntType;
import main.ast.type.primitiveType.StringType;
import main.symbolTable.SymbolTable;
import main.symbolTable.SymbolTableHandlerItem;
import main.symbolTable.itemException.ItemNotFoundException;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class ActorCodeGenerator extends CodeGenerator {


    public ActorCodeGenerator(String outputDir) {
        super(outputDir);
    }

    @Override
    public void visit(Program program) {
        //noinspection ResultOfMethodCallIgnored
        new File(outputDirectory).mkdirs();

        ArrayList<ActorDeclaration> actors = program.getActors();
        if (actors != null) {
            for (ActorDeclaration actor : actors) {
                actor.accept(this);
            }
        }

    }

    @Override
    public void visit(ActorDeclaration actorDeclaration) {
        super.visit(actorDeclaration);

        try {
            currentWriter = new PrintWriter(String.format("%s/%s.j", this.outputDirectory, currentActorName), StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.err.println(String.format("Error opening %s.j", currentActorName));
            currentActorName = "";
            return;
        }

        currentWriter.println(String.format(".class public %s", currentActorName));
        currentWriter.println(".super Actor\n");

        StringBuilder knownActorTypes = new StringBuilder();
        ArrayList<VarDeclaration> varDecs = actorDeclaration.getKnownActors();
        if (varDecs != null) {
            for (VarDeclaration varDec : varDecs) {
                varDec.accept(this);
                knownActorTypes.append(getJasminType(varDec.getType()));
            }
        }

        varDecs = actorDeclaration.getActorVars();
        if (varDecs != null) {
            for (VarDeclaration varDec : varDecs) {
                varDec.accept(this);
            }
        }

        currentWriter.println("\n.method public <init>(I)V");
        currentWriter.println(".limit stack 1000");
        currentWriter.println(".limit locals 1000");
        currentWriter.println("\taload_0");
        currentWriter.println("\tiload_1");
        currentWriter.println("\tinvokespecial Actor/<init>(I)V"); // Actor is parent name

        currentWriter.println(";");
        currentWriter.println("; set default value for int, boolean and string actorVars:");
        currentWriter.println(";");

        varDecs = actorDeclaration.getActorVars();
        if (varDecs != null) {
            for (VarDeclaration varDec : varDecs) {
                Type type = varDec.getType();
                currentWriter.println("\taload_0");
                if (type instanceof IntType || type instanceof BooleanType) {
                    currentWriter.println("\ticonst_0");
                } else if (type instanceof StringType) {
                    currentWriter.println("\tldc \"\"");
                }
                if (!(type instanceof ArrayType)) {
                    currentWriter.println(
                            String.format("\tputfield %s/%s %s",
                                    currentActorName,
                                    varDec.getIdentifier().getName(),
                                    getJasminType(type)
                            )
                    );
                } else {
                    int size = ((ArrayType) type).getSize();
                    currentWriter.println(String.format("\tldc %d", size));
                    currentWriter.println("\tnewarray int");
                    currentWriter.println(
                            String.format("\tputfield %s/%s %s",
                                    currentActorName,
                                    varDec.getIdentifier().getName(),
                                    getJasminType(type)
                            )
                    );
                    for(int i = 0; i < size; i++) {
                        currentWriter.println("\taload_0");
                        currentWriter.println(String.format("\tldc %d", i));
                        currentWriter.println("\tldc 0");
                        currentWriter.println("\tiastore");
                    }
                }
            }
        }

        currentWriter.println(";");
        currentWriter.println("; variable initialization end.");
        currentWriter.println(";");

        currentWriter.println("\treturn");
        currentWriter.println(".end method");


        currentWriter.println(
                String.format("\n.method public setKnownActors(%s)V",
                        knownActorTypes.toString()
                )
        );
        currentWriter.println(".limit stack 1000");
        currentWriter.println(".limit locals 1000");

        varDecs = actorDeclaration.getKnownActors();
        if (varDecs != null) {
            int i = 1;
            for (VarDeclaration varDec : varDecs) {
                currentWriter.println("\taload_0");
                currentWriter.println(String.format("\taload %d", i++));
                currentWriter.println(
                        String.format("\tputfield %s/%s %s",
                                currentActorName,
                                varDec.getIdentifier().getName(),
                                getJasminType(varDec.getType())
                        )
                );
            }
        }

        currentWriter.println("\treturn");
        currentWriter.println(".end method");

        InitHandlerDeclaration initHandlerDeclaration = actorDeclaration.getInitHandler();
        if (initHandlerDeclaration != null) {
            initHandlerDeclaration.accept(this);
        }

        ArrayList<MsgHandlerDeclaration> msgHandlerDecs = actorDeclaration.getMsgHandlers();
        if (msgHandlerDecs != null) {
            for (MsgHandlerDeclaration msgHandlerDec : msgHandlerDecs) {
                msgHandlerDec.accept(this);
            }
        }

        currentWriter.close();
        currentActorName = "";
    }

    @Override
    public void visit(HandlerDeclaration handlerDeclaration) {
        currentScopeVariablesIndex.clear();
        try {
            SymbolTable.top = (
                    (SymbolTableHandlerItem) SymbolTable.top.get(
                            SymbolTableHandlerItem.STARTKEY + handlerDeclaration.getName().getName()
                    )
            ).getHandlerSymbolTable();
        } catch (ItemNotFoundException exp) {
            System.err.println("handler symbol table not found");
        }

        int index = 1;
        StringBuilder argTypes = new StringBuilder();
        if (!(handlerDeclaration instanceof InitHandlerDeclaration)) {
            argTypes.append("LActor;");
            index++;
        }
        ArrayList<VarDeclaration> varDecs = handlerDeclaration.getArgs();
        if (varDecs != null) {
            for (VarDeclaration varDec : varDecs) {
                argTypes.append(getJasminType(varDec.getType()));
                currentScopeVariablesIndex.put(varDec.getIdentifier().getName(), index++);
            }
        }

        String handlerName = handlerDeclaration.getName().getName();
        if (!(handlerDeclaration instanceof InitHandlerDeclaration)) {
            String messageName = String.format("%s_%s", currentActorName, handlerName);
            currentWriter.println(
                    String.format("\n.method public send_%s(%s)V",
                            handlerName,
                            argTypes.toString()
                    )
            );
            currentWriter.println(".limit stack 1000");
            currentWriter.println(".limit locals 1000");

            currentWriter.println("\taload_0");
            currentWriter.println(String.format("\tnew %s", messageName));
            currentWriter.println("\tdup");

            currentWriter.println("\taload_0"); // this
            currentWriter.println("\taload_1"); // sender
            varDecs = handlerDeclaration.getArgs();
            if (varDecs != null) {
                int i = 2;
                for (VarDeclaration varDec : varDecs) {
                    currentWriter.println(
                            String.format("\t%s %s",
                                    getJasminLoadType(varDec.getType()),
                                    Integer.toString(i++)
                            )
                    ); // args
                }
            }

            currentWriter.println(
                    String.format("\tinvokespecial %s/<init>(%s)V",
                            messageName,
                            String.format("L%s;%s", currentActorName, argTypes)
                    )
            );
            currentWriter.println(
                    String.format("\tinvokevirtual %s/send(LMessage;)V",
                            currentActorName
                    )
            );
            currentWriter.println("\treturn");
            currentWriter.println(".end method");
        }

        currentWriter.println(
                String.format("\n.method public %s(%s)V",
                        handlerName,
                        argTypes.toString()
                )
        );
        currentWriter.println(".limit stack 1000");
        currentWriter.println(".limit locals 1000");

        /*---------------------- local vars ----------------------------------------------------------*/

        currentWriter.println(";");
        currentWriter.println("; set default value for int, boolean and string localVars:");
        currentWriter.println(";");

        varDecs = handlerDeclaration.getLocalVars();
        if (varDecs != null) {
            for (VarDeclaration varDec : varDecs) {
                currentScopeVariablesIndex.put(varDec.getIdentifier().getName(), index);
                Type type = varDec.getType();
                if (type instanceof IntType || type instanceof BooleanType) {
                    System.err.println(type);
//                    currentWriter.println("\ticonst_0");
//                    currentWriter.println(String.format("\tistore %d", index));
                } else if (type instanceof StringType) {
                    currentWriter.println("\tldc \"\"");
                    currentWriter.println(String.format("\tastore %d", index));
                } else if (type instanceof ArrayType) {
                    int size = ((ArrayType) type).getSize();
                    currentWriter.println(String.format("\tldc %d", size));
                    currentWriter.println("\tnewarray int");
                    currentWriter.println(String.format("\tastore %d", index));
                    for(int i = 0; i < size; i++) {
                        currentWriter.println(String.format("\taload %d", index));
                        currentWriter.println(String.format("\tldc %d", i));
                        currentWriter.println("\tldc 0");
                        currentWriter.println("\tiastore");
                    }
                }
                index++;
            }
        }

        currentWriter.println(";");
        currentWriter.println("; variable initialization end.");
        currentWriter.println(";");

        /*---------------------- end local vars ----------------------------------------------------------*/

        ArrayList<Statement> stmts = handlerDeclaration.getBody();
        if (stmts != null) {
            for (Statement stmt : stmts) {
                stmt.accept(this);
            }
        }

        currentWriter.println("\treturn");
        currentWriter.println(".end method");

        SymbolTable.top = SymbolTable.top.getPreSymbolTable();
    }
}
