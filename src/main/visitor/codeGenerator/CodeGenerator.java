package main.visitor.codeGenerator;

import main.ast.node.declaration.ActorDeclaration;
import main.ast.node.declaration.VarDeclaration;
import main.ast.node.expression.*;
import main.ast.node.expression.operators.BinaryOperator;
import main.ast.node.expression.operators.UnaryOperator;
import main.ast.node.expression.values.BooleanValue;
import main.ast.node.expression.values.IntValue;
import main.ast.node.expression.values.StringValue;
import main.ast.node.statement.*;
import main.ast.type.Type;
import main.ast.type.actorType.ActorType;
import main.ast.type.arrayType.ArrayType;
import main.ast.type.primitiveType.BooleanType;
import main.ast.type.primitiveType.IntType;
import main.ast.type.primitiveType.StringType;
import main.symbolTable.SymbolTable;
import main.symbolTable.SymbolTableActorItem;
import main.symbolTable.itemException.ItemNotFoundException;
import main.symbolTable.symbolTableVariableItem.SymbolTableActorVariableItem;
import main.symbolTable.symbolTableVariableItem.SymbolTableKnownActorItem;
import main.symbolTable.symbolTableVariableItem.SymbolTableVariableItem;
import main.visitor.VisitorImpl;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

abstract class CodeGenerator extends VisitorImpl {
    String outputDirectory;
    static PrintWriter currentWriter;
    String currentActorName;
    private static int labelCounter = 0;
    HashMap<String, Integer> currentScopeVariablesIndex = new HashMap<>();
    private Stack<String> breakLabels = new Stack<>();
    private Stack<String> continueLabels = new Stack<>();

    CodeGenerator(String outputDir) {
        this.outputDirectory = outputDir;
    }

    String getJasminType(Type type) {
        if (type instanceof IntType)
            return "I";
        else if (type instanceof ArrayType)
            return "[I";
        else if (type instanceof BooleanType)
            return "Z";
        else if (type instanceof StringType)
            return "Ljava/lang/String;";
        else if (type instanceof ActorType)
            return String.format("L%s;", ((ActorType) type).getName().getName());
        else
            return "ERROR Type";
    }

    String getJasminLoadType(Type type) {
        if (type instanceof IntType || type instanceof BooleanType)
            return "iload";
        else
            return "aload";
    }

    private String getJasminStoreType(Type type) {
        if (type instanceof IntType || type instanceof BooleanType)
            return "istore";
        else
            return "astore";
    }


    private void compareStatements(String operator, String condition) {
        String operatorBegin = String.format("%s_%s", operator, Integer.toString(labelCounter));
        String operatorEnd = String.format("%s_%s", new StringBuilder(operator).reverse().toString(), Integer.toString(labelCounter));
        String scopeEnd = String.format("end_%s", Integer.toString(labelCounter++));

        currentWriter.println(String.format("\tif_icmp%s %s", condition, operatorEnd));
        currentWriter.println(String.format("%s:", operatorBegin));
        currentWriter.println("\ticonst_0");
        currentWriter.println(String.format("\tgoto %s", scopeEnd));
        currentWriter.println(String.format("%s:", operatorEnd));
        currentWriter.println("\ticonst_1");
        currentWriter.println(String.format("%s:", scopeEnd));
    }


    private void checkEquality(String operator, String condition, Expression value) {
        Type type = value.getType();

        if (type instanceof IntType || type instanceof BooleanType)
            compareStatements(operator, condition);
        else {
            if (type instanceof StringType)
                currentWriter.println("\tinvokevirtual java/lang/String/equals(Ljava/lang/Object;)Z");
            else
                currentWriter.println("\tinvokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z");

            if (operator.equals("neq"))
                notStatement();
        }
    }

    private void notStatement() {
        String scopeEnd = String.format("begin_notUnary_%s", Integer.toString(labelCounter));
        String statementEnd = String.format("end_notUnary_%s", Integer.toString(labelCounter++));

        currentWriter.println(String.format("\tifeq %s", scopeEnd));
        currentWriter.println("\ticonst_0");
        currentWriter.println(String.format("\tgoto %s", statementEnd));

        currentWriter.println(String.format("%s:", scopeEnd));
        currentWriter.println("\ticonst_1");

        currentWriter.println(String.format("%s:", statementEnd));
    }

    private boolean isActorVar(Identifier identifier) {
        SymbolTableVariableItem symbolTableVariableItem;
        try {
            symbolTableVariableItem = (SymbolTableVariableItem) SymbolTable.top.get(String.format("%s%s", SymbolTableVariableItem.STARTKEY, identifier.getName()));
        } catch (ItemNotFoundException ignored) {
            symbolTableVariableItem = null;
        }
        return symbolTableVariableItem instanceof SymbolTableActorVariableItem || symbolTableVariableItem instanceof SymbolTableKnownActorItem;
    }

    @Override
    public void visit(ActorDeclaration actorDeclaration) {
        currentActorName = actorDeclaration.getName().getName();
        try {
            SymbolTable.top = (
                    (SymbolTableActorItem) SymbolTable.root.get(
                            SymbolTableActorItem.STARTKEY + actorDeclaration.getName().getName()
                    )
            ).getActorSymbolTable();
        } catch (ItemNotFoundException exp) {
            System.err.println("actor symbol table not found");
        }
    }

    @Override
    public void visit(VarDeclaration varDeclaration) {
        currentWriter.println(
                String.format(".field private %s %s",
                        varDeclaration.getIdentifier().getName(),
                        getJasminType(varDeclaration.getType())
                )
        );
    }

    @Override
    public void visit(UnaryExpression unaryExpression) {
        Expression unaryExpressionOperand = unaryExpression.getOperand();
        visitExpression(unaryExpressionOperand);

        UnaryOperator unaryOperator = unaryExpression.getUnaryOperator();

        if (unaryOperator == UnaryOperator.not) {
            notStatement();
        } else if (unaryOperator == UnaryOperator.minus) {
            currentWriter.println("\tineg");
        } else {
            BinaryOperator binaryOperator;
            if (unaryOperator == UnaryOperator.preinc || unaryOperator == UnaryOperator.postinc) {
                binaryOperator = BinaryOperator.add;
            } else /*if (unaryOperator == UnaryOperator.predec || unaryOperator == UnaryOperator.postdec)*/ {
                binaryOperator = BinaryOperator.sub;
            }
            BinaryExpression binaryExpression = new BinaryExpression(
                    unaryExpressionOperand, new IntValue(1, new IntType()), binaryOperator
            );
            Assign assign = new Assign(unaryExpressionOperand, binaryExpression);
            assign.accept(this);
            if (unaryOperator == UnaryOperator.preinc || unaryOperator == UnaryOperator.predec) {
                currentWriter.println("\ticonst_1");
                if (unaryOperator == UnaryOperator.preinc) {
                    currentWriter.println("\tiadd");
                } else {
                    currentWriter.println("\tisub");
                }
            }
        }
    }

    private void handleShortCircuit(BinaryExpression binaryExpression) {
        String expressionMiddle = String.format("middle_expression_%s", Integer.toString(labelCounter));
        String expressionEnd = String.format("end_expression_%s", Integer.toString(labelCounter++));

        Expression left = binaryExpression.getLeft();
        Expression right = binaryExpression.getRight();
        BinaryOperator binaryOperator = binaryExpression.getBinaryOperator();

        left.accept(this);
        if (binaryOperator == BinaryOperator.and) {
            currentWriter.println(String.format("\tifeq %s", expressionMiddle));
        } else if (binaryOperator == BinaryOperator.or) {
            currentWriter.println(String.format("\tifne %s", expressionMiddle));
        }

        right.accept(this);
        currentWriter.println(String.format("\tgoto %s", expressionEnd));

        currentWriter.println(String.format("%s:", expressionMiddle));
        if (binaryOperator == BinaryOperator.and) {
            currentWriter.println("\ticonst_0");
        } else if (binaryOperator == BinaryOperator.or) {
            currentWriter.println("\ticonst_1");
        }

        currentWriter.println(String.format("%s:", expressionEnd));
    }

    @Override
    public void visit(BinaryExpression binaryExpression) {
        BinaryOperator binaryOperator = binaryExpression.getBinaryOperator();

        if (binaryOperator == BinaryOperator.and || binaryOperator == BinaryOperator.or) {
            handleShortCircuit(binaryExpression);
            return;
        }

        Expression left = binaryExpression.getLeft();
        Expression right = binaryExpression.getRight();
        if (binaryOperator != BinaryOperator.assign) {
            left.accept(this);
            right.accept(this);
        }

        switch (binaryOperator) {
            case add:
                currentWriter.println("\tiadd");
                break;

            case sub:
                currentWriter.println("\tisub");
                break;

            case mult:
                currentWriter.println("\timul");
                break;

            case div:
                currentWriter.println("\tidiv");
                break;

            case mod:
                currentWriter.println("\tirem");
                break;

            case eq:
                checkEquality("eq", "eq", left);
                break;

            case neq:
                checkEquality("neq", "ne", left);
                break;

            case lt:
                compareStatements("lt", "lt");
                break;

            case gt:
                compareStatements("gt", "gt");
                break;

            case assign:
                Assign tempAssign = new Assign(left, right);
                tempAssign.accept(this);
                left.accept(this);
                break;

            default:
                break;
        }
    }

    @Override
    public void visit(ArrayCall arrayCall) {
        putArrayCallRefOnStack(arrayCall);
        currentWriter.println("\tiaload");
    }

    private void putArrayCallRefOnStack(ArrayCall arrayCall) {
        Expression instance = arrayCall.getArrayInstance();
        instance.accept(this);

        Expression index = arrayCall.getIndex();
        index.accept(this);
    }

    @Override
    public void visit(ActorVarAccess actorVarAccess) {
        visitExpression(actorVarAccess.getSelf());

        Identifier id = actorVarAccess.getVariable();
        currentWriter.println(
                String.format("\tgetfield %s/%s %s",
                        currentActorName,
                        id.getName(),
                        getJasminType(id.getType())
                )
        );
    }

    @Override
    public void visit(Identifier identifier) {
        if (isActorVar(identifier)) {
            currentWriter.println("\taload_0");
            currentWriter.println(
                    String.format("\tgetfield %s/%s %s",
                            currentActorName,
                            identifier.getName(),
                            getJasminType(identifier.getType())
                    )
            );
        } else {
            currentWriter.println(
                    String.format("\t%s %d",
                            getJasminLoadType(identifier.getType()),
                            currentScopeVariablesIndex.get(identifier.getName())
                    )
            );
        }
    }

    @Override
    public void visit(Self self) {
        currentWriter.println("\taload_0");
    }

    @Override
    public void visit(Sender sender) {
        currentWriter.println("\taload_1");
    }

    @Override
    public void visit(BooleanValue value) {
        currentWriter.println(String.format("\ticonst_%d", value.getIntConstant()));
    }

    @Override
    public void visit(IntValue value) {
        currentWriter.println(String.format("\tldc %d", value.getConstant()));
    }

    @Override
    public void visit(StringValue value) {
        currentWriter.println(String.format("\tldc %s", value.getConstant()));
    }

    @Override
    public void visit(Block block) {
        ArrayList<Statement> stmts = block.getStatements();
        if (stmts != null) {
            for (Statement stmt : stmts) {
                stmt.accept(this);
            }
        }
    }

    @Override
    public void visit(Conditional conditional) {
        String scopeBegin = String.format("if_%s", Integer.toString(labelCounter));
        String scopeEnd = String.format("fi_%s", Integer.toString(labelCounter));
        String statementEnd = String.format("else_%s", Integer.toString(labelCounter++));

        Expression expr = conditional.getExpression();
        if (expr != null) {
            expr.accept(this);
        }

        currentWriter.println(String.format("\tifeq %s", scopeEnd));

        currentWriter.println(String.format("%s:", scopeBegin));

        Statement thenBody = conditional.getThenBody();
        if (thenBody != null) {
            thenBody.accept(this);
        }

        currentWriter.println(String.format("\tgoto %s", statementEnd));
        currentWriter.println(String.format("%s:", scopeEnd));

        Statement elseBody = conditional.getElseBody();
        if (elseBody != null) {
            elseBody.accept(this);
        }

        currentWriter.println(String.format("%s:", statementEnd));
    }

    @Override
    public void visit(For loop) {
        visitStatement(loop.getInitialize());

        String scopeBegin = String.format("loop_%s", Integer.toString(labelCounter));
        String scopeEnd = String.format("pool_%s", Integer.toString(labelCounter++));

        breakLabels.push(scopeEnd);
        continueLabels.push(scopeBegin);

        currentWriter.println(String.format("%s:", scopeBegin));

        visitExpression(loop.getCondition());

        currentWriter.println(String.format("\tifeq %s", scopeEnd));

        visitStatement(loop.getBody());

        visitStatement(loop.getUpdate());

        currentWriter.println(String.format("\tgoto %s", scopeBegin));

        currentWriter.println(String.format("%s:", scopeEnd));

        breakLabels.pop();
        continueLabels.pop();
    }

    @Override
    public void visit(Break breakLoop) {
        currentWriter.println(String.format("\tgoto %s", breakLabels.peek()));
    }

    @Override
    public void visit(Continue continueLoop) {
        currentWriter.println(String.format("\tgoto %s", continueLabels.peek()));
    }

    @Override
    public void visit(MsgHandlerCall msgHandlerCall) {
        Expression instance = msgHandlerCall.getInstance();
        String instanceType = "";
        if (instance instanceof Self) {
            currentWriter.println("\taload_0");
            instanceType = currentActorName;
        } else if (instance instanceof Sender) {
            currentWriter.println("\taload_1");
            instanceType = "Actor";
        } else if (instance instanceof Identifier) {
            currentWriter.println("\taload_0");
            currentWriter.println(
                    String.format("\tgetfield %s/%s %s",
                            currentActorName,
                            ((Identifier) instance).getName(),
                            getJasminType(instance.getType())
                    )
            );
            instanceType = ((ActorType) instance.getType()).getName().getName();
        }

        currentWriter.println("\taload_0"); // this as first argument
        StringBuilder argTypes = new StringBuilder();
        argTypes.append("LActor;");
        ArrayList<Expression> args = msgHandlerCall.getArgs();
        if (args != null) {
            for (Expression arg : args) {
                arg.accept(this);
                argTypes.append(getJasminType(arg.getType()));
            }
        }

        currentWriter.println(
                String.format("\tinvokevirtual %s/send_%s(%s)V",
                        instanceType,
                        msgHandlerCall.getMsgHandlerName().getName(),
                        argTypes.toString()
                )
        );
    }

    @Override
    public void visit(Print print) {
        Expression arg = print.getArg();
        currentWriter.println("\tgetstatic java/lang/System/out Ljava/io/PrintStream;");

        arg.accept(this);

        Type type = arg.getType();
        if (type instanceof ArrayType) {
            currentWriter.println(
                    String.format("\tinvokestatic java/util/Arrays/toString(%s)Ljava/lang/String;",
                            getJasminType(type)
                    )
            );
            currentWriter.println(
                    String.format("\tinvokevirtual java/io/PrintStream/println(%s)V",
                            "Ljava/lang/String;"
                    )
            );
            return;
        }

        currentWriter.println(
                String.format("\tinvokevirtual java/io/PrintStream/println(%s)V",
                        getJasminType(type)
                )
        );
    }

    @Override
    public void visit(Assign assign) {
        Expression left = assign.getlValue();
        Expression right = assign.getrValue();

        if (left instanceof ArrayCall) {
            putArrayCallRefOnStack((ArrayCall) left);
            right.accept(this);
            currentWriter.println("\tiastore");
        } else if (left instanceof ActorVarAccess) {
            currentWriter.println("\taload_0");
            right.accept(this);
            Identifier id = ((ActorVarAccess) left).getVariable();
            currentWriter.println(
                    String.format("\tputfield %s/%s %s",
                            currentActorName,
                            id.getName(),
                            getJasminType(id.getType())
                    )
            );
        } else if (left instanceof Identifier) {
            boolean isLeftSideActorVar = isActorVar((Identifier) left);
            Identifier leftId = (Identifier) left;
            if (isLeftSideActorVar) {
                currentWriter.println("\taload_0");
            }
            right.accept(this);
            if (isLeftSideActorVar) {
                currentWriter.println(
                        String.format("\tputfield %s/%s %s",
                                currentActorName,
                                leftId.getName(),
                                getJasminType(left.getType())
                        )
                );
            } else {
                currentWriter.println(
                        String.format("\t%s %d",
                                getJasminStoreType(left.getType()),
                                currentScopeVariablesIndex.get(leftId.getName())
                        )
                );
            }
        } else {
            System.err.println("Unhandled Assign left value type");
        }
    }
}
