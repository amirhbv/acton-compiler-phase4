package main.visitor.codeGenerator;

import main.ast.node.Program;
import main.ast.node.declaration.ActorDeclaration;
import main.ast.node.declaration.VarDeclaration;
import main.ast.node.declaration.handler.HandlerDeclaration;
import main.ast.node.declaration.handler.InitHandlerDeclaration;
import main.ast.node.declaration.handler.MsgHandlerDeclaration;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class DefaultCodeGenerator extends CodeGenerator {

    private Set<String> handlerNames = new HashSet<>();

    public DefaultCodeGenerator(String outputDir) {
        super(outputDir);
    }

    private void createMessageClass() {
        try {
            currentWriter = new PrintWriter(String.format("%s/Message.j", this.outputDirectory), StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.err.println("Error opening Message.j");
            e.printStackTrace();
            return;
        }

        currentWriter.println(".class public abstract Message");
        currentWriter.println(".super java/lang/Object");

        currentWriter.println("\n.method public <init>()V");
        currentWriter.println(".limit stack 1");
        currentWriter.println(".limit locals 1");
        currentWriter.println("\taload_0");
        currentWriter.println("\tinvokespecial java/lang/Object/<init>()V");
        currentWriter.println("\treturn");
        currentWriter.println(".end method");

        currentWriter.println("\n.method public abstract execute()V");
        currentWriter.println(".end method");

        currentWriter.close();
    }

    private void createActorClass() {
        try {
            currentWriter = new PrintWriter(String.format("%s/Actor.j", this.outputDirectory), StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.err.println("Error opening Actor.j");
            e.printStackTrace();
            return;
        }

        currentWriter.println(".class public Actor");
        currentWriter.println(".super DefaultActor");

        currentWriter.println("\n.field private queue Ljava/util/ArrayList;");
        currentWriter.println(".signature \"Ljava/util/ArrayList<LMessage;>;\"");
        currentWriter.println(".end field");
        currentWriter.println(".field private lock Ljava/util/concurrent/locks/ReentrantLock;");
        currentWriter.println(".end field");
        currentWriter.println(".field queueSize I");
        currentWriter.println(".end field");

        currentWriter.println("\n.method public <init>(I)V");
        currentWriter.println(".limit stack 3");
        currentWriter.println(".limit locals 2");
        currentWriter.println("\taload_0");
        currentWriter.println("\tinvokespecial DefaultActor/<init>()V");
        currentWriter.println("\taload_0");
        currentWriter.println("\tnew java/util/ArrayList");
        currentWriter.println("\tdup");
        currentWriter.println("\tinvokespecial java/util/ArrayList/<init>()V");
        currentWriter.println("\tputfield Actor/queue Ljava/util/ArrayList;");
        currentWriter.println("\taload_0");
        currentWriter.println("\tnew java/util/concurrent/locks/ReentrantLock");
        currentWriter.println("\tdup");
        currentWriter.println("\tinvokespecial java/util/concurrent/locks/ReentrantLock/<init>()V");
        currentWriter.println("\tputfield Actor/lock Ljava/util/concurrent/locks/ReentrantLock;");
        currentWriter.println("\taload_0");
        currentWriter.println("\tiload_1");
        currentWriter.println("\tputfield Actor/queueSize I");
        currentWriter.println("\treturn");
        currentWriter.println(".end method");

        currentWriter.println("\n.method public run()V");
        currentWriter.println(".limit stack 2");
        currentWriter.println(".limit locals 2");
        currentWriter.println("Label0:");
        currentWriter.println("\taconst_null");
        currentWriter.println("\tastore_1");
        currentWriter.println("\taload_0");
        currentWriter.println("\tgetfield Actor/lock Ljava/util/concurrent/locks/ReentrantLock;");
        currentWriter.println("\tinvokevirtual java/util/concurrent/locks/ReentrantLock/lock()V");
        currentWriter.println("\taload_0");
        currentWriter.println("\tgetfield Actor/queue Ljava/util/ArrayList;");
        currentWriter.println("\tinvokevirtual java/util/ArrayList/isEmpty()Z");
        currentWriter.println("\tifne Label31");
        currentWriter.println("\taload_0");
        currentWriter.println("\tgetfield Actor/queue Ljava/util/ArrayList;");
        currentWriter.println("\ticonst_0");
        currentWriter.println("\tinvokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;");
        currentWriter.println("\tcheckcast Message");
        currentWriter.println("\tastore_1");
        currentWriter.println("Label31:");
        currentWriter.println("\taload_0");
        currentWriter.println("\tgetfield Actor/lock Ljava/util/concurrent/locks/ReentrantLock;");
        currentWriter.println("\tinvokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V");
        currentWriter.println("\taload_1");
        currentWriter.println("\tifnull Label46");
        currentWriter.println("\taload_1");
        currentWriter.println("\tinvokevirtual Message/execute()V");
        currentWriter.println("Label46:");
        currentWriter.println("\tgoto Label0");
        currentWriter.println(".end method");

        currentWriter.println("\n.method public send(LMessage;)V");
        currentWriter.println(".limit stack 2");
        currentWriter.println(".limit locals 2");
        currentWriter.println("\taload_0");
        currentWriter.println("\tgetfield Actor/lock Ljava/util/concurrent/locks/ReentrantLock;");
        currentWriter.println("\tinvokevirtual java/util/concurrent/locks/ReentrantLock/lock()V");
        currentWriter.println("\taload_0");
        currentWriter.println("\tgetfield Actor/queue Ljava/util/ArrayList;");
        currentWriter.println("\tinvokevirtual java/util/ArrayList/size()I");
        currentWriter.println("\taload_0");
        currentWriter.println("\tgetfield Actor/queueSize I");
        currentWriter.println("\tif_icmpge Label30");
        currentWriter.println("\taload_0");
        currentWriter.println("\tgetfield Actor/queue Ljava/util/ArrayList;");
        currentWriter.println("\taload_1");
        currentWriter.println("\tinvokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z");
        currentWriter.println("\tpop");
        currentWriter.println("Label30:");
        currentWriter.println("\taload_0");
        currentWriter.println("\tgetfield Actor/lock Ljava/util/concurrent/locks/ReentrantLock;");
        currentWriter.println("\tinvokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V");
        currentWriter.println("\treturn");
        currentWriter.println(".end method");

        currentWriter.close();
    }

    @Override
    public void visit(Program program) {
        //noinspection ResultOfMethodCallIgnored
        new File(outputDirectory).mkdirs();

        try {
            currentWriter = new PrintWriter(String.format("%s/DefaultActor.j", this.outputDirectory), StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.err.println("Error opening DefaultActor.j");
            e.printStackTrace();
            return;
        }

        currentWriter.println(".class public DefaultActor");
        currentWriter.println(".super java/lang/Thread");

        currentWriter.println("\n.method public <init>()V");
        currentWriter.println(".limit stack 1");
        currentWriter.println(".limit locals 1");
        currentWriter.println("\taload_0");
        currentWriter.println("\tinvokespecial java/lang/Thread/<init>()V");
        currentWriter.println("\treturn");
        currentWriter.println(".end method");

        ArrayList<ActorDeclaration> actors = program.getActors();
        if (actors != null) {
            for (ActorDeclaration actor : actors) {
                actor.accept(this);
            }
        }

        currentWriter.close();

        this.createMessageClass();
        this.createActorClass();
    }

    @Override
    public void visit(ActorDeclaration actorDeclaration) {

        ArrayList<MsgHandlerDeclaration> msgHandlerDecs = actorDeclaration.getMsgHandlers();
        if (msgHandlerDecs != null) {
            for (MsgHandlerDeclaration msgHandlerDec : msgHandlerDecs) {
                msgHandlerDec.accept(this);
            }
        }

    }

    @Override
    public void visit(HandlerDeclaration handlerDeclaration) {
        String handlerName = handlerDeclaration.getName().getName();
        if (handlerNames.contains(handlerName)) {
            return;
        }
        handlerNames.add(handlerName);

        StringBuilder argTypes = new StringBuilder();
        if (!(handlerDeclaration instanceof InitHandlerDeclaration)) {
            argTypes.append("LActor;");
        }
        ArrayList<VarDeclaration> varDecs = handlerDeclaration.getArgs();
        if (varDecs != null) {
            for (VarDeclaration varDec : varDecs) {
                argTypes.append(getJasminType(varDec.getType()));
            }
        }

        currentWriter.println(
                String.format("\n.method public send_%s(%s)V",
                        handlerName,
                        argTypes.toString()
                )
        );
        currentWriter.println(".limit stack 2");
        currentWriter.println(".limit locals 3");
        currentWriter.println("\tgetstatic java/lang/System/out Ljava/io/PrintStream;");
        currentWriter.println(String.format("\tldc \"there is no msghandler named %s in sender\"", handlerName));
        currentWriter.println("\tinvokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
        currentWriter.println("\treturn");
        currentWriter.println(".end method");
    }
}
