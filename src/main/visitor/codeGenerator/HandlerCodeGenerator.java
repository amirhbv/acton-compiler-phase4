package main.visitor.codeGenerator;

import main.ast.node.Program;
import main.ast.node.declaration.ActorDeclaration;
import main.ast.node.declaration.VarDeclaration;
import main.ast.node.declaration.handler.HandlerDeclaration;
import main.ast.node.declaration.handler.MsgHandlerDeclaration;
import main.ast.type.Type;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class HandlerCodeGenerator extends CodeGenerator {

    private String currentActorName;

    public HandlerCodeGenerator(String outputDir) {
        super(outputDir);
    }

    @Override
    public void visit(Program program) {
        //noinspection ResultOfMethodCallIgnored
        new File(outputDirectory).mkdirs();

        ArrayList<ActorDeclaration> actors = program.getActors();
        if (actors != null) {
            for (ActorDeclaration actor : actors) {
                actor.accept(this);
            }
        }
    }

    @Override
    public void visit(ActorDeclaration actorDeclaration) {
        currentActorName = actorDeclaration.getName().getName();

        ArrayList<MsgHandlerDeclaration> msgHandlerDecs = actorDeclaration.getMsgHandlers();
        if (msgHandlerDecs != null) {
            for (MsgHandlerDeclaration msgHandlerDec : msgHandlerDecs) {
                msgHandlerDec.accept(this);
            }
        }

        currentActorName = "";
    }

    @Override
    public void visit(HandlerDeclaration handlerDeclaration) {
        String handlerName = handlerDeclaration.getName().getName();
        String messageName = String.format("%s_%s", currentActorName, handlerName);
        String receiverType = String.format("L%s;", currentActorName);

        try {
            currentWriter = new PrintWriter(String.format("%s/%s.j", this.outputDirectory, messageName), StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.err.println(String.format("Error opening %s.j", messageName));
            return;
        }

        currentWriter.println(String.format(".class public %s", messageName));
        currentWriter.println(".super Message");
        currentWriter.println("\n.field private sender LActor;");
        currentWriter.println(String.format(".field private receiver %s", receiverType));

        StringBuilder argTypes = new StringBuilder();
        argTypes.append("LActor;");

        ArrayList<VarDeclaration> varDecs = handlerDeclaration.getArgs();
        if (varDecs != null) {
            for (VarDeclaration varDec : varDecs) {
                varDec.accept(this);
                argTypes.append(getJasminType(varDec.getType()));
            }
        }

        currentWriter.println(
                String.format("\n.method public <init>(%s)V",
                        String.format("%s%s", receiverType, argTypes)
                )
        );
        currentWriter.println(".limit stack 1000");
        currentWriter.println(".limit locals 1000");

        currentWriter.println("\taload_0");
        currentWriter.println("\tinvokespecial Message/<init>()V");

        currentWriter.println("\taload_0");
        currentWriter.println("\taload_1");
        currentWriter.println(
                String.format("\tputfield %s/receiver %s",
                        messageName,
                        receiverType
                )
        );

        currentWriter.println("\taload_0");
        currentWriter.println("\taload_2");
        currentWriter.println(
                String.format("\tputfield %s/sender LActor;",
                        messageName
                )
        );

        varDecs = handlerDeclaration.getArgs();
        if (varDecs != null) {
            int i = 3;
            for (VarDeclaration varDec : varDecs) {
                Type varDecType = varDec.getType();
                currentWriter.println("\taload_0");
                currentWriter.println(String.format("\t%s %d", getJasminLoadType(varDecType), i++));
                currentWriter.println(
                        String.format("\tputfield %s/%s %s",
                                messageName,
                                varDec.getIdentifier().getName(),
                                getJasminType(varDecType)
                        )
                );
            }
        }

        currentWriter.println("\treturn");
        currentWriter.println(".end method");

        currentWriter.println("\n.method public execute()V");
        currentWriter.println(".limit stack 1000");
        currentWriter.println(".limit locals 1000");

        currentWriter.println("\taload_0");
        currentWriter.println(
                String.format("\tgetfield %s/receiver %s",
                        messageName,
                        receiverType
                )
        );

        currentWriter.println("\taload_0");
        currentWriter.println(
                String.format("\tgetfield %s/sender LActor;",
                        messageName
                )
        );

        varDecs = handlerDeclaration.getArgs();
        if (varDecs != null) {
            for (VarDeclaration varDec : varDecs) {
                currentWriter.println("\taload_0");
                currentWriter.println(
                        String.format("\tgetfield %s/%s %s",
                                messageName,
                                varDec.getIdentifier().getName(),
                                getJasminType(varDec.getType())
                        )
                );
            }
        }

        currentWriter.println(
                String.format("invokevirtual %s/%s(%s)V",
                        currentActorName,
                        handlerName,
                        argTypes
                )
        );

        currentWriter.println("\treturn");
        currentWriter.println(".end method");

        currentWriter.close();
    }
}
