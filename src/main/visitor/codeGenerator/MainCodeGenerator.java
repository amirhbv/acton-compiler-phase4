package main.visitor.codeGenerator;

import main.ast.node.Main;
import main.ast.node.Program;
import main.ast.node.declaration.ActorDeclaration;
import main.ast.node.declaration.ActorInstantiation;
import main.ast.node.declaration.VarDeclaration;
import main.ast.node.declaration.handler.InitHandlerDeclaration;
import main.ast.node.expression.Expression;
import main.ast.node.expression.Identifier;
import main.ast.type.actorType.ActorType;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

enum TraverseState {
    ActorInstantiation, SettingKnownActors, ActorInitialization, Starting
}

public class MainCodeGenerator extends CodeGenerator {
    private TraverseState traverseState;
    private ArrayList<ActorDeclaration> actorDeclarations;

    public MainCodeGenerator(String outputDir) {
        super(outputDir);
    }

    private ActorDeclaration getActorDeclaration(String s) {
        for (ActorDeclaration actordec : actorDeclarations) {
            if (actordec.getName().getName().equals(s))
                return actordec;
        }
        return null;
    }

    @Override
    public void visit(Program program) {
        //noinspection ResultOfMethodCallIgnored
        new File(outputDirectory).mkdirs();

        actorDeclarations = program.getActors();

        Main main = program.getMain();
        if (main != null) {
            main.accept(this);
        }
    }

    @Override
    public void visit(Main main) {

        try {
            currentWriter = new PrintWriter(String.format("%s/Main.j", this.outputDirectory), StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.err.println("Error opening Main.j");
            return;
        }

        currentWriter.println(".class public Main");
        currentWriter.println(".super java/lang/Object");

        currentWriter.println("\n.method public <init>()V");
        currentWriter.println(".limit stack 1");
        currentWriter.println(".limit locals 1");
        currentWriter.println("\taload_0");
        currentWriter.println("\tinvokespecial java/lang/Object/<init>()V");
        currentWriter.println("\treturn");
        currentWriter.println(".end method");

        currentWriter.println("\n.method public static main([Ljava/lang/String;)V");
        currentWriter.println(".limit stack 1000");
        currentWriter.println(".limit locals 1000");

        currentScopeVariablesIndex.clear();
        ArrayList<ActorInstantiation> actorInstantiations = main.getMainActors();
        if (actorInstantiations != null) {
            int index = 1;
            for (ActorInstantiation actorInstantiation : actorInstantiations) {
                currentScopeVariablesIndex.put(
                        actorInstantiation.getIdentifier().getName(),
                        index++
                );
            }

            for (TraverseState traverseState : TraverseState.values()) {
                this.traverseState = traverseState;
                for (ActorInstantiation actorInstantiation : actorInstantiations) {
                    actorInstantiation.accept(this);
                }
            }
        }


        currentWriter.println("\treturn");
        currentWriter.println(".end method");

        currentWriter.close();
    }

    @Override
    public void visit(ActorInstantiation actorInstantiation) {

        ActorType actorType = ((ActorType) actorInstantiation.getType());
        String actorTypeName = actorType.getName().getName();
        ActorDeclaration actorDeclaration = getActorDeclaration(actorTypeName);
        assert actorDeclaration != null;

        switch (traverseState) {
            case ActorInstantiation:
                instantiateActor(actorInstantiation, actorDeclaration, actorTypeName);
                break;
            case SettingKnownActors:
                setKnownActors(actorInstantiation, actorDeclaration, actorTypeName);
                break;
            case ActorInitialization:
                initializeActor(actorInstantiation, actorDeclaration, actorTypeName);
                break;
            case Starting:
                startActor(actorInstantiation, actorTypeName);
                break;
        }
    }

    private void instantiateActor(
            ActorInstantiation actorInstantiation, ActorDeclaration actorDeclaration, String actorTypeName) {

        currentWriter.println(String.format("\tnew %s", actorTypeName));
        currentWriter.println("\tdup");
        currentWriter.println(String.format("\tldc %d", actorDeclaration.getQueueSize()));
        currentWriter.println(String.format("\tinvokespecial %s/<init>(I)V", actorTypeName));
        currentWriter.println(
                String.format("\tastore %d",
                        currentScopeVariablesIndex.get(actorInstantiation.getIdentifier().getName())
                )
        );
    }

    private void setKnownActors(
            ActorInstantiation actorInstantiation, ActorDeclaration actorDeclaration, String actorTypeName) {

        currentWriter.println(
                String.format("\taload %d",
                        currentScopeVariablesIndex.get(actorInstantiation.getIdentifier().getName())
                )
        );

        ArrayList<Identifier> knownActorArgs = actorInstantiation.getKnownActors();
        if (knownActorArgs != null) {
            for (Identifier knownActor : knownActorArgs) {
                currentWriter.println(
                        String.format("\taload %d",
                                currentScopeVariablesIndex.get(knownActor.getName())
                        )
                );
            }
        }

        ArrayList<VarDeclaration> knownActors = actorDeclaration.getKnownActors();
        StringBuilder knownActorTypes = new StringBuilder();
        if (knownActors != null) {
            for (VarDeclaration knownActor : knownActors) {
                knownActorTypes.append(getJasminType(knownActor.getType()));
            }
        }
        currentWriter.println(
                String.format("\tinvokevirtual %s/setKnownActors(%s)V",
                        actorTypeName,
                        knownActorTypes
                )
        );
    }

    private void initializeActor(
            ActorInstantiation actorInstantiation, ActorDeclaration actorDeclaration, String actorTypeName) {


        InitHandlerDeclaration initHandlerDeclaration = actorDeclaration.getInitHandler();
        if (initHandlerDeclaration == null) {
            return;
        }
        StringBuilder argTypes = new StringBuilder();
        ArrayList<VarDeclaration> varDecs = initHandlerDeclaration.getArgs();
        if (varDecs != null) {
            for (VarDeclaration varDec : varDecs) {
                argTypes.append(getJasminType(varDec.getType()));
            }
        }

        currentWriter.println(
                String.format("\taload %d",
                        currentScopeVariablesIndex.get(actorInstantiation.getIdentifier().getName())
                )
        );

        ArrayList<Expression> initArgs = actorInstantiation.getInitArgs();
        if (initArgs != null) {
            for (Expression initArg : initArgs) {
                visitExpression(initArg);
            }
        }

        currentWriter.println(
                String.format("\tinvokevirtual %s/initial(%s)V",
                        actorTypeName,
                        argTypes
                )
        );
    }

    private void startActor(
            ActorInstantiation actorInstantiation, String actorTypeName) {

        currentWriter.println(
                String.format("\taload %d",
                        currentScopeVariablesIndex.get(actorInstantiation.getIdentifier().getName())
                )
        );

        currentWriter.println(
                String.format("\tinvokevirtual %s/start()V", actorTypeName)
        );
    }
}
